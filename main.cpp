/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: vinicius
 *
 * Created on 14 de Setembro de 2018, 08:41
 */

#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <vector>
#include <map>

#include "Algorithm.h"

using namespace std;

// function prototypes
int menu();
void initArray();
void printValues(vector<double> array);
void printModa(vector<double> array);
void printMediana(vector<double> array);
void printMediaGeometrica(vector<double> array);
void printMediaAritmetica(vector<double> array);
void printMediaPonderada(vector<double> array);
void printDesvioPadrao(vector<double> array);

// globals variables
Algorithm algorithm;
vector<double> array;
int n = 0;

/**
 * 
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char** argv) {
    system("clear");
    srand(time(NULL));

    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐      Quantos números deseja calcular?   ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " \n ➸ Quantidade:  ";
    cin >> n;
    system("clear");

    initArray();
    
    cout << "\n moda: ";
    for (vector<double>::iterator it = array.begin(); it != array.end(); it++) {
        cout << (*it) << ", ";
    }

//    printValues(array);
//    printModa(array);
//    printMediana(array);
//    printMediaGeometrica(array);
//    printMediaAritmetica(array);
//    printMediaPonderada(array);
//    printDesvioPadrao(array);

    cout << endl << endl;
    return 0;
}

void initArray() {
    if (n <= 0)
        exit(EXIT_SUCCESS);

    for (int i = 0; i < n; i++)
        array.push_back((rand() % 100 + 1));

    sort(array.begin(), array.end());
}

/**
 * 
 * @param array
 * @return 
 */
void printValues(vector<double> array) {

    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                 VALORES                 ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ array: ";
    for (vector<double>::iterator it = array.begin(); it != array.end(); it++)
        cout << (*it) << ", ";
    cout << endl;
}

/**
 * 
 * @param array
 */
void printModa(vector<double> array) {

    vector<double> moda = algorithm.moda(array);

    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                   MODA                  ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ Moda: ";
    for (vector<double>::iterator it = array.begin(); it != array.end(); it++)
        cout << (*it) << ", ";
    cout << endl;
}

/**
 * 
 * @param array
 */
void printMediana(vector<double> array) {

    double mediana = algorithm.mediana(array);

    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                  MEDIANA                ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ Mediana: " << mediana << endl;
}

/**
 * 
 * @param array
 */
void printMediaGeometrica(vector<double> array) {

    double mG = algorithm.mediaGeometrica(array);

    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐              MÉDIA GEOMÉTRICA           ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ Média Geométrica: " << mG << endl;
}

/**
 * 
 * @param array
 */
void printMediaAritmetica(vector<double> array) {

    double mA = algorithm.mediaAritmetica(array);

    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐              MÉDIA ARITMÉTICA           ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ Média Aritmética: " << mA << endl;
}

void printMediaPonderada(vector<double> array) {

    map<double, int> map;

    for (vector<double>::iterator it = array.begin(); it != array.end(); it++)
        map.insert(pair<double, int> ((*it), (rand() % 100 + 1)));
    double mP = algorithm.mediaPonderada(map);

    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐               MÉDIA PONDERADA           ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ Média Ponderada: " << mP << endl;
}

void printDesvioPadrao(vector<double> array) {

    double desvioPadrao = algorithm.desvioPadrao(array);

    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐               DESVIO PADRÃO             ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ Desvio Padrão: " << desvioPadrao << endl;
}