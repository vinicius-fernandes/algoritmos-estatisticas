/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Algorithm.h
 * Author: vinicius
 *
 * Created on 14 de Setembro de 2018, 08:41
 */

#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <map>

using namespace std;

class Algorithm {
public:

    /**
     * 
     */
    Algorithm();

    /**
     * 
     */
    virtual ~Algorithm();

    /**
     * 
     * @param array
     * @return 
     */
    vector<double> moda(vector<double> array);

    /**
     * 
     * @param array
     * @return 
     */
    double mediana(vector<double> array);

    /**
     * 
     * @param array
     * @return 
     */
    double mediaGeometrica(vector<double> array);

    /**
     * 
     * @param array
     * @return 
     */
    double mediaAritmetica(vector<double> array);
    
    /**
     * 
     * @param values
     * @return 
     */
    double mediaPonderada(map<double, int> values);
    
    /**
     * 
     * @param valor
     * @param raiz
     * @return 
     */
    double desvioPadrao(vector<double> array);

private:

    /**
     * 
     * @param valor
     * @param raiz
     * @return 
     */
    double enesimaRaiz(double valor, int raiz);
    
    /**
     * 
     * @param array
     * @return 
     */
    double variancia(vector<double> array);

};

#endif /* ALGORITHM_H */

